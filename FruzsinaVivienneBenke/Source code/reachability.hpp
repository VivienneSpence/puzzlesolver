#include <vector>
#include <functional>
#include <iostream>
#include <string>
#include <array>
#include <list>
#include <map>
#include <stack>
#include <algorithm>


/*
 * The library can be used by all 3 puzzles as requested in requirement 8.
 */

//For readability purposes
using namespace std;

/*
 * A template for a class called Node that has a state, a trace, a label and cost.
 * It is used to keep track of all the traces.
 * It has a default constructor and a constructor that takes a state of any type (i.e can be frogs, actors or state_t)
 * and a vector of traces.
 */
template<typename T, class Cost>
class Node {

public:
    T state;
    vector<T> trace;
    bool isDiscovered;
    Cost cost;

    Node() {}

    Node(T state, vector<T> trace) : state(state), trace(trace) {
        isDiscovered = false;
    }

    virtual ~Node() {

    }
};

/*
 * The successor generator function needed in the cpps.
 * It takes any type of container (i.e vector, list or deque) and a transitions function.
 * It returns a vector of the possible successors.
 * Generate successors as requested in requirement 1.
 * Support any iterable containers as requested in requirement 7.
 */
template<typename Container, class T>
function<vector<T>(T)> successors(function<Container(T)> transitions) {
    return [&transitions](T state) {
        vector<T> successors{};
        auto transitionFunctions = transitions(state);
        for (auto &t : transitionFunctions) {
            auto success = state;
            t(success);
            successors.push_back(success);
        }
        return successors;
    };
}

/*
 * An enum class defining the search order to be used for finding a solution.
 * To support different search orders - requirement 4.
 */
enum class search_order_t {
    breadth_first,
    depth_first
};

/*
 * A struct for the state space.
 * I has 2 methods: chek(...) and popstate(...)
 */
template<typename T, class Cost = bool>
struct state_space_t {

    /*
     * The main check function that generates the solution.
     * It implements the pseudo code provided in the assignment (to some extent).
     * For requirement 2.
     */
    vector<T> check(function<bool(const T &)> checkState, search_order_t order = search_order_t::breadth_first) {

        //Declaration of a waiting list, a trace list and current cost
        vector<Node<T, Cost>> waiting;
        vector<T> trace;
        Cost currentCost;

        // Create a node out of the starting state
        Node<T, Cost> startNode = Node<T, Cost>(start, trace);
        // Add the created node to the waiting list
        waiting.push_back(startNode);

        //while the waiting list is not empty
        while (waiting.size() > 0) {
            // Get the current node that we will be working on from the waiting list.
            // This is done by calling the popstate method and providing it with the waiting list and the desired order
            Node<T, Cost> currentNode = popstate(waiting, order);
            //Remove the currentNode from the waiting list using the erase-remove idiom to make sure we don't check this node again
            waiting.erase(remove(waiting.begin(), waiting.end(), currentNode), waiting.end());

            //Check if the state space has cost (only in the case of family.cpp)
            if (hasCost == true) {
                //If cost exists and the current node is the start node calculate the cost with the start node
                //And set the cost of the current node equal to currentCost
                if (currentNode.state == start) {
                    currentCost = costFunction(currentNode.state, startCost);
                    currentNode.cost = currentCost;
                }
                    //Else is the currentNode is not the start node calculate cost with current node
                    //And set the cost of the current node equal to currentCost
                else {
                    currentCost = costFunction(currentNode.state, currentCost);
                    currentNode.cost = currentCost;
                }
            }

            //Check if the current node that we are working with corresponds to the final state
            // To print out traces for requirement 3.
            if (checkState(currentNode.state)) {
                //If yes add the state of the current node to the traces of the current node.
                currentNode.trace.push_back(currentNode.state);
                //Print out the size of the final trace
                cout << "Final trace size:" << currentNode.trace.size() << "\n";
                //Call the logOutput method to print out the solution found.
                vector<T> finalTrace = logOutput(currentNode.trace, currentNode);
                //return traces
                return trace;
            }
            //In case the current node is not the final state and its hasn't been discovered yet
            if (currentNode.isDiscovered != true) {
                //Set the current node's label to true
                currentNode.isDiscovered = true;
                //Generate the successors of the current node
                vector<T> children = successorGeneratorFunction(currentNode.state);
                //If the current node has successors we add it to the trace.
                //This check is necessary because if the current node doesn't have successors and we know that its not the
                //final state we know that its a dead end.
                if (children.size() != 0) {
                    currentNode.trace.push_back(currentNode.state);
                    cout << "Creating trace: " << currentNode.trace << "\n";
                    //For each children we create a Node and if they are not already in the waiting list we add them.
                    //We also make sure that the child we want to add to the waiting list doesn't violate the puzzle invariant (for requirement 5)
                    for (auto child: children) {
                        Node<T, Cost> childNode = Node<T, Cost>(child, currentNode.trace);
                        if (find(waiting.begin(), waiting.end(), childNode) == waiting.end() &&
                            puzzleInvariant(child)) {
                            //If the hasCost flag is set to true we calculate the cost of the childNode and add add it to the waiting list.
                            //Otherwise we just add it to the waiting list.
                            if (hasCost) {
                                currentCost = costFunction(childNode.state, currentCost);
                                childNode.cost = currentCost;
                                waiting.push_back(childNode);
                            }
                            waiting.push_back(childNode);
                        }
                    }
                }
            }

        }
        //If there is no solution return an empty vector.
        vector<T> noSolution;
        return noSolution;
    }


    /*
     * The popstate() method can be customized to pick the first state (resulting in breadth-first order) or the last state (resulting in depth-first order).
     * It takes a vector of nodes and a search order.
     * It return either the first or the last element based on the searchorder declared.
     */
    auto popstate(vector<Node<T, Cost>> v, search_order_t order) {

        if (order == search_order_t::breadth_first) {
            auto next = v.front();
            return next;
        } else if (order == search_order_t::depth_first) {
            auto next = v.back();
            return next;
        } else {
            return v.front();
        }

    }

    /*
     * Below we create the state_space_t.
     * There are two possibilities for it. One without the cost and one with cost.
     * This was necessary because of family.cpp
     * Both versions have a start state, a successor generator function, a puzzle invariant and a cost flag.
     * Only the second version has a starting cost and a cost function.
     */
    T start;
    function<vector<T>(T)> successorGeneratorFunction;
    function<bool(T)> puzzleInvariant;

    Cost startCost;
    //Cost function implemented for requirement 6.
    function<Cost(T, Cost)> costFunction;
    bool hasCost;

    state_space_t(
            const T start,
            function<vector<T>(T)> func,
            function<bool(T)> predicate = [](T st) { return true; }) {
        this->start = start;
        this->successorGeneratorFunction = func;
        this->puzzleInvariant = predicate;
        this->hasCost = false;
    }

    state_space_t(
            const T start,
            const Cost initialCost,
            function<std::vector<T>(T)> func,
            const std::function<bool(T)> predicate = [](T st) { return true; },
            function<Cost(T, Cost)> costFunc = [](T st, Cost cst) { return cst; }) {
        this->start = start;
        this->successorGeneratorFunction = func;
        this->hasCost = true;
        this->startCost = initialCost;
        this->costFunction = costFunc;
        this->puzzleInvariant = predicate;
    }

    state_space_t() = delete;

    ~state_space_t() = default;

};


/*
 * Functions that help printing the traces.
 * These are overloads for the << operator to be able to print out the elements of a vector, array and deques.
 * There is also an overload for the == operator because I needed to see if two Node<T, Cost> were equal or not.
 */
template<typename T>
ostream &operator<<(ostream &os, const vector<T> &v) {
    for (int i = 0; i < v.size(); ++i) {
        os << v[i];
    }
    return os;
}


template<typename T, size_t arrSize>
ostream &operator<<(ostream &os, const array<T, arrSize> &v) {
    for (size_t i = 0; i < v.size(); ++i) {
        os << v[i];
    }
    return os;
}

template<typename T>
ostream &operator<<(ostream &os, const deque<T> &v) {
    for (size_t i = 0; i < v.size(); ++i) {
        os << v[i];
    }
    return os;
}

template<typename T, typename enable_if<is_enum<T>::value>::type * = nullptr>
ostream &operator<<(ostream &os, T &objClass) {
    return os << (char) (objClass);

}

template<typename T, class Cost>
bool operator==(const Node<T, Cost> &a, const Node<T, Cost> &b) {
    return (a.state == b.state && a.trace == b.trace);
}

/*
 * A generic log method that works for all 3 cpps.
 */
template<typename T, class Cost>
vector<T> logOutput(vector<T> trace, Node<T, Cost> node) {

    for (auto r: trace) {
        // cout << "A trace of " << node.state.size() << " stones: " << r << "\n";
        cout << "Solution trace: " << r << "\n";
    }
    return trace;
};

/*
 * The log() method required by family.cpp.
 */
template<typename T>
void log(T element) {
    std::cout << element << '\n';
}